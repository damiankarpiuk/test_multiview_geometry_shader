#version 430

out vec2 texCoord;

void main()
{
    switch(gl_VertexID)
    {
        case 0:
            gl_Position = vec4(-1, -1, 0, 1);
            texCoord = vec2(0, 0);
            break;
        case 1:
            gl_Position = vec4(-1, 1, 0, 1);
            texCoord = vec2(0, 1);
            break;
        case 2:
            gl_Position = vec4(1, -1, 0, 1);
            texCoord = vec2(1, 0);
            break;
        case 3:
            gl_Position = vec4(1, 1, 0, 1);
            texCoord = vec2(1, 1);
            break;
    }
}
