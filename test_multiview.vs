#version 430

#extension GL_OVR_multiview2: enable
layout(num_views=2) in;

out vec4 color;

void main()
{
    switch(gl_VertexID)
    {
        case 0:
            gl_Position = vec4(-0.5, -0.5, 0, 1);
            break;
        case 1:
            gl_Position = vec4(0.5, -0.5, 0, 1);
            break;
        case 2:
            gl_Position = vec4(0, 0.5, 0, 1);
            break;
    }

    if(gl_ViewID_OVR > 0)
        gl_Position.y = -gl_Position.y;

    color = vec4(1,1,1,1);
}
