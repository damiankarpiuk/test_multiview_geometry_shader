#version 430

#extension GL_EXT_multiview_tessellation_geometry_shader : enable
layout(num_views = 2) in;

out vec4 color;

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

void main()
{
    gl_Position = gl_in[0].gl_Position + vec4(0, 0.25, 0, 0);
    color = vec4(1,0,0,1);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position + vec4(0, 0.25, 0, 0);
    color = vec4(0,1,0,1);
    EmitVertex();

    gl_Position = gl_in[2].gl_Position + vec4(0, 0.25, 0, 0);
    color = vec4(0,0,1,1);
    EmitVertex();

    EndPrimitive();
}
