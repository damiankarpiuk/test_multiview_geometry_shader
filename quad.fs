#version 430

layout(binding=0) uniform sampler2DArray texArray;

layout(location=0) out vec4 outColor;

in vec2 texCoord;

uniform int u_arrayIndex;

void main()
{
    outColor = texture(texArray, vec3(texCoord, u_arrayIndex));
}
