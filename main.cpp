#include "glad.h"
#include "GLFW/glfw3.h"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

GLuint loadShader(const std::string &filename, GLenum shaderType)
{
    std::ifstream shaderFile(filename, std::ios::binary);

    if (!shaderFile.is_open())
    {
        std::cerr << "Can't find shader " << filename << "\n";
        return 0;
    }

    std::string shaderSource{ std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>() };
    const GLchar *sourcePtr = (const GLchar *)shaderSource.c_str();

    GLuint shader = glCreateShader(shaderType);

    // Get strings for glShaderSource.
    glShaderSource(shader, 1, &sourcePtr, 0);

    glCompileShader(shader);

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::string errorLog(maxLength, (char)0);
        glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

        std::cerr << "Error compiling " << filename << ":\n";
        std::cerr << errorLog << "\n";

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shader); // Don't leak the shader.
        return 0;
    }

    return shader;
}

GLuint createProgram(GLuint vert, GLuint geom, GLuint frag)
{
    GLuint program = glCreateProgram();

    // Attach our shaders to our program
    if (vert) glAttachShader(program, vert);
    if (geom) glAttachShader(program, geom);
    if (frag) glAttachShader(program, frag);

    // Link our program
    glLinkProgram(program);

    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::string infoLog(maxLength, (char)0);
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

        // We don't need the program anymore.
        glDeleteProgram(program);

        // Use the infoLog as you see fit.
        std::cerr << "Error linking program:\n";
        std::cerr << infoLog << "\n";

        // In this simple program, we'll just leave
        return 0;
    }

    // Always detach shaders after a successful link.
    if (vert) glDetachShader(program, vert);
    if (geom) glDetachShader(program, geom);
    if (frag) glDetachShader(program, frag);

    return program;
}

void messageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "OpenGL message: " << message << "\n";
}

GLuint multiViewProgramWithGeom = 0;
GLuint multiViewProgramNoGeom = 0;
GLuint texturedQuadProgram = 0;

GLuint renderTargetTextureArray = 0;
GLuint multiviewFBO = 0;

constexpr int rttWidth  = 1024;
constexpr int rttHeight = 1024;

void setupRendering()
{
    // setup debug callback
    //
    glDebugMessageCallback(messageCallback, nullptr);
    
    // load shaders
    //
    GLuint fragShader = loadShader("test.fs", GL_FRAGMENT_SHADER);
    GLuint multiviewVertShader = loadShader("test_multiview.vs", GL_VERTEX_SHADER);
    GLuint multiviewGeomShader = loadShader("test_multiview.gs", GL_GEOMETRY_SHADER);
    multiViewProgramWithGeom = createProgram(multiviewVertShader, multiviewGeomShader, fragShader);
    multiViewProgramNoGeom = createProgram(multiviewVertShader, 0, fragShader);

    GLuint texturedQuadVertShader = loadShader("quad.vs", GL_VERTEX_SHADER);
    GLuint texturedQuadFragShader = loadShader("quad.fs", GL_FRAGMENT_SHADER);
    texturedQuadProgram = createProgram(texturedQuadVertShader, 0, texturedQuadFragShader);

    glDeleteShader(fragShader);
    glDeleteShader(multiviewVertShader);
    glDeleteShader(multiviewGeomShader);
    glDeleteShader(texturedQuadVertShader);
    glDeleteShader(texturedQuadFragShader);

    // setup rtt
    //
    glGenTextures(1, &renderTargetTextureArray);
    glBindTexture(GL_TEXTURE_2D_ARRAY, renderTargetTextureArray);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, rttWidth, rttHeight, 2);
    glBindTextureUnit(0, renderTargetTextureArray);

    glGenFramebuffers(1, &multiviewFBO);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, multiviewFBO);
    glFramebufferTextureMultiviewOVR(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderTargetTextureArray, 0, 0, 2);
}

GLFWwindow* window;

void render()
{
    int timeInSeconds = static_cast<int>(glfwGetTime());

    // render multi-view
    //
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, multiviewFBO);
    glViewport(0, 0, rttWidth, rttHeight);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    switch (timeInSeconds % 4)
    {
    case 0: // without geometry shader
        glUseProgram(multiViewProgramNoGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        break;
    case 1: // with GS
        glUseProgram(multiViewProgramWithGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        break;
    case 2: //without GS, then with GS
        glUseProgram(multiViewProgramNoGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glUseProgram(multiViewProgramWithGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        break;
    case 3: //with GS, then without GS
        glUseProgram(multiViewProgramWithGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glUseProgram(multiViewProgramNoGeom);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        break;
    }  

    // show results
    //
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    glViewport(0, 0, w, h);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(texturedQuadProgram);

    glViewport(0, h / 4, w / 2, h / 2);
    glUniform1i(glGetUniformLocation(texturedQuadProgram, "u_arrayIndex"), 0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glViewport(w / 2, h / 4, w / 2, h / 2);
    glUniform1i(glGetUniformLocation(texturedQuadProgram, "u_arrayIndex"), 1);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void cleanup()
{
    glDeleteProgram(multiViewProgramWithGeom);
    glDeleteProgram(multiViewProgramNoGeom);
    glDeleteProgram(texturedQuadProgram);

    glDeleteTextures(1, &renderTargetTextureArray);
    glDeleteFramebuffers(1, &multiviewFBO);
}

int main(void)
{
    /* Initialize the library */
    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Test", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (!gladLoadGL())
    {
        return -1;
    }

    setupRendering();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        render();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    cleanup();

    glfwTerminate();
    return 0;
}
